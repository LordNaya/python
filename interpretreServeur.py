#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
import json
import dictionnaireConst as const



class InterpreteServeur():

    def __init__(self):
        pass

    def obtenirMsgServeurSelonJson(self, msgDuServeur):
            msgReponse = None
            msgErreur = ''

            try: #plante sur le telechargement a cause du format bytes que le serveur envoit
                msgDuServeur = json.loads(msgDuServeur)
            except:
                return (const.ERREUR_TRAITEMENT_TEXTE + ' ' + msgDuServeur)
            
 ################# BIG ASS IF ELIF ##################
            for key in msgDuServeur:
                         
                # Traitement de { 'salutation': 'bonjourClient' }   
                if key == const.SALUTATION and msgDuServeur[key] == const.BONJOUR_CLIENT:
                    msgReponse = "Oui"
                
                # Traitement de { 'nomServeur': '' }          
                elif key == const.NOM_SERVEUR:
                    msgReponse = msgDuServeur[key]

                # Traitement de { 'reponse': 'Ok' }   
                elif key == const.REPONSE and msgDuServeur[key] == const.OK:
                    msgReponse = "Oui"

                # Traitement de { 'reponse': 'Oui' }   
                elif key == const.REPONSE and msgDuServeur[key] == const.OUI:
                    msgReponse = "Oui"
                
                # Traitement de { 'reponse': 'Non' }   
                elif key == const.REPONSE and msgDuServeur[key] == const.NON:
                    msgReponse = "Non"

                # Traitement de { 'reponse': 'bye' }   
                elif key == const.REPONSE and msgDuServeur[key] == const.BYE:
                    msgReponse = "Bye."      

################# Message Dossier ##################

                # Traitement de { 'reponse': 'erreurDossierInexistant' }           
                elif key == const.REPONSE and msgDuServeur[key] == const.ERREUR_DOSSIER_INEXISTANT:
                    msgReponse = "Ce dossier n'existe pas"

                # Traitement de { 'reponse': 'erreurDossierLecture' }   
                elif key == const.REPONSE and msgDuServeur[key] == const.ERREUR_DOSSIER_LECTURE:
                    msgReponse = "Ce dossier ne peut pas être lu."               

                # Traitement de { 'reponse': 'erreurDossierExiste' }   
                elif key == const.REPONSE and msgDuServeur[key] == const.ERREUR_DOSSIER_EXISTE:
                    msgReponse = "Ce dossier existe déjà."         
    
    ################# Message Fichier ##################

                # Traitement de { 'reponse': 'erreurFichierInexistant' }   
                elif key == const.REPONSE and msgDuServeur[key] == const.ERREUR_FICHIER_INEXISTANT:
                    msgReponse = "Ce fichier n'existe pas."  

                # Traitement de { 'reponse': 'erreurFichierLecture' }   
                elif key == const.REPONSE and msgDuServeur[key] == const.ERREUR_FICHIER_LECTURE:
                    msgReponse = "Ce fichier ne peut pas être lu."  
                
                # Traitement de { 'reponse': 'erreurFichierExiste' }   
                elif key == const.REPONSE and msgDuServeur[key] == const.ERREUR_FICHIER_EXISTE:
                    msgReponse = "Ce fichier existe déjà."  

                # Traitement de {'fichier': {'signature': '...', 'contenu': '...', 'date': '...'}}
                elif key == const.FICHIER:
                    msgReponse = "Ok"


 ################# Message obtention d'information  ##################
                
                # Traitement de { "listeDossiers": {"dossiers": ["d1/d2", "d1/d3", "d1/d4"]} }   
                elif key == const.LISTE_DOSSIERS:

                    msgReponse = self.obtenirListeDossier(msgDuServeur)

                else:
                   msgErreur = "c'est louche"

            if msgReponse is None:
                return ('', "KKchose a chier dans pelle msgReponse est vide")
                
            return (msgReponse, msgErreur)
    
    


# Traitements particuliers retour serveur

    def obtenirListeDossier(self, jsonServeur):
        retour = ""
        data = jsonServeur["listeDossiers"]["dossiers"]
        for value in data:
            retour += value
            retour += "\n" 

        return retour



########### test area #############

# banane = InterpreteServeur()

# msg = { "listeDossiers": {"dossiers": ["d1/d2", "d1/d3", "d1/d4"]} } 

# banane.obtenirListeDossier(msg)

# print(patate.interpreteServeur(json.dumps(msg)))
