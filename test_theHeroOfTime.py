#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
from theHeroOfTime import Hero_of_Time as timeHero

class TestHeroOfTime(unittest.TestCase):

    def test_obtenirDateHeure_enToutTemps_RetourneDateHeureEnFormatString(self):
        retour = timeHero.obtenirDateHeure(self)

        self.assertIsInstance(retour, str)

   

if __name__ == '__main__':
    unittest.main()
