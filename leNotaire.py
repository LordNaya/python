#!/usr/bin/python3
# -*- coding: utf-8 -*-

import hashlib
import base64 

class Notaire:

    def __init__(self):
        pass
    
    def obtenir_signature_contenu(self, fichier):
        with open(fichier, mode='rb') as file:
            fichier_image = file.read()
            file.close()
        
        m = hashlib.sha256()
        m.update(fichier_image)
        
        return m.hexdigest()

    def obtenirContenuString(self, fichier):
        with open(fichier, mode='rb') as file:
            fichier_image = file.read()
            file.close()
        
        contentuByte = base64.b64encode(fichier_image)
        return contentuByte.decode("utf-8")
