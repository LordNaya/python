#!/usr/bin/python3
# -*- coding: utf-8 -*-

from mock_python import remove
import unittest
import unittest.mock as mock

class TestClientTp4(unittest.TestCase):

    @mock.patch('mock_python.os.path')
    @mock.patch('mock_python.os')
    def setUp(self, mock_os, mock_path):
        mock_path.isFile.return_value = False

        remove("fichier1")
        self.assertFalse(mock_os.remove.called, "Failed to not remove the file")

        mock_os.remove.assert_called_with("fichier1")


    # def test_CommandeSupprimerFIchier_AvecFichierValide_AppelleSuppression(self):
    #     fichier1 = "fichier1"

        




if __name__ == '__main__':
    unittest.main()        
