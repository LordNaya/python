#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Constantes pour la classe Serveur

NOM_SERVEUR_ACTUEL = "TP 4 Dropbox 2.0"
DOSSIER_RACINE = './'
BONJOUR = 'Bonjour'
AUCUNE = 'Aucune'
DOSSIER_INEXISTANT = 'Dossier inexistant'
ERREUR_LECTURE_DOSSIER = 'Erreur de lecture du dossier'
ERREUR_DOSSIER_EXISTE = 'Erreur dossier existe'
ERREUR_DOSSIER_INEXISTANT = 'Erreur dossier inexistant'
ERREUR_FICHIER_EXISTE = 'Erreur fichier existe'
ERREUR_SIGNATURE = 'Erreur signature'
ERREUR_FICHIER_INEXISTANT = 'Erreur fichier inexistant'
ERREUR_FICHIER_LECTURE = 'Erreur fichier lecture'
ERREUR_DOSSIER_LECTURE = 'Erreur dossier lecture'
NON = 'Non'
OUI = 'Oui'
QUITTER = 'Quitter'

# Constantes pour la classe Protocole

BONJOUR_SERVEUR = 'bonjourServeur'
BONJOUR_CLIENT = 'bonjourClient'
SALUTATION = 'salutation'
NOM_SERVEUR = 'nomServeur'
LISTE_DOSSIERS = 'listeDossiers'
DOSSIER = 'dossier'
DOSSIERS = 'dossiers'
ERREUR_DOSSIER_INEXISTANT = 'erreurDossierInexistant'
REPONSE = 'reponse'
ERREUR_DOSSIER_LECTURE = 'erreurDossierLecture'
LISTE_FICHIERS = 'listeFichiers'
FICHIER = 'fichier'
FICHIERS = 'fichiers'
OK = 'ok'
ERREUR_DOSSIER_EXISTE = 'erreurDossierExiste'
ERREUR_FICHIER_EXISTE = 'erreurFichierExiste'
ERREUR_SIGNATURE = 'erreurSignature'
SIGNATURE = 'signature'
CONTENU = 'contenu'
DATE = 'date'
ERREUR_FICHIER_INEXISTANT = 'erreurFichierInexistant'
ERREUR_FICHIER_LECTURE = 'erreurFichierLecture'
BYE = 'bye'
CONTENU_XML_RECU_SERVEUR_NON_RECONNU = 'Le contenu XML reçu du serveur est non-reconnu ou mal formaté.'
ERREUR_TRAITEMENT_TEXTE = 'Erreur de traitement du texte'
ERREUR_TRAITEMENT_SALUTATION_CLIENT = 'Erreur de traitement de la salutation du client'
QUESTION_NOM_SERVEUR = 'questionNomServeur'
ERREUR_TRAITEMENT_NOM_SERVEUR = 'Erreur de traitement du nom du serveur.'
QUESTION_LISTE_DOSSIERS = 'questionListeDossiers'
ERREUR_TRAITEMENT_LISTE_DOSSIER = 'Erreur de traitement de la liste du dossier'
QUESTION_LISTE_FICHIERS = 'questionListeFichiers'
ERREUR_TRAITEMENT_LISTE_FICHIERS = 'Erreur de traitement de la liste du fichiers'
CREER_DOSSIER = 'creerDossier'
ERREUR_TRAITEMENT_CREATION_DOSSIER = 'Erreur de traitement de la création du dossier.'
TELEVERSER_FICHIER = 'televerserFichier'
NOM = 'nom'
DOSSIER = 'dossier'
ERREUR_TRAITEMENT_TELEVERSEMENT_FICHIERS = 'Erreur de traitement du téléversement de fichiers'
TELECHARGER_FICHIER = 'telechargerFichier'
ERREUR_TRAITEMENT_TELECHARGEMENT_FICHIERS = 'Erreur de traitement du téléchargement de fichiers'
SUPPRIMER_FICHIER = 'supprimerFichier'
SUPPRIMER_DOSSIER = 'supprimerDossier'
ERREUR_TRAITEMENT_SUPPRESSION_DOSSIER = 'Erreur de traitement de la suppression du dossier.'
QUESTION_FICHIER_RECENT = 'questionFichierRecent'
ERREUR_RECHERCHE_FICHIER_PLUS_RECENT = 'Erreur de recherche du fichier le plus récent.'
QUESTION_FICHIER_IDENTIQUE = 'questionFichierIdentique'
ERREUR_RECHERCHE_COMPARAISON_FICHIERS = 'Erreur de recherche de comparaison de fichiers.'
ACTION = 'action'
QUITTER = 'quitter'
DEMANDE_ARRET_COMMUNICATION = 'Demande d\'arrêt de la communication.'
ERREUR_ARRET_SERVEUR = 'Erreur de l\'arrêt du serveur.'
CONTENU_JSON_RECU_NONRECONNU_MAL_FORMATE = 'Le contenu JSON reçu est non-reconnu ou mal formaté.'
#!/usr/bin/python3
# -*- coding: utf-8 -*-
import socket

import xml.dom.minidom
import json
import sys



# from twisted.internet.protocol import Protocol
from twisted.protocols.basic import LineReceiver

MAX_RECV = 1024 * 1024 * 512

MAX_NOMBRE_RECV = 100

CODE_FIN_LIGNE = "\r\n"

###############################################################################


class Interface_client(LineReceiver):
    """Représentation du client.
       Effet de bord."""

    # Limite entre 4 et 6 Mo de la taille des envois
    MAX_LENGTH = 16384 * 64 * 5

    def connectionMade(self):
        self.ip = self.transport.getPeer()
        print("Serveur s'est connecté à " + str(self.ip))
        print("Voici les messages venant du client:")

    def connectionLost(self, reason):
        print("La connexion au client " + str(self.ip) + " a été coupée.")
        # Seul endroit où le serveur s'arrête.
        self.transport.reactor.stop()

    # Méthode de lineReceived
    def lineReceived(self, msgApp_bytes):
        "Méthode de lineReceived"

        # Conversion bytes en string.
        msgApp = msgApp_bytes.decode("UTF-8")

        if msgApp and msgApp != "\n" and msgApp != CODE_FIN_LIGNE:
            # On coupe la longueur de la chaîne pour éviter l'affichage trop long.
            msgAppImp = (msgApp[:100] + '...') if len(msgApp) > 100 else msgApp
            print("Client: " + msgAppImp.strip())
        else:
            print('Message vide du client. Fermeture de la connexion.')
            self.transport.loseConnection()

        (msgAppReponse, msgErreur) = self.factory.proto.interpreteServeur(msgApp, self.factory)

        if msgAppReponse == self.factory.proto.gen_quitter():
            print(msgErreur)
            self.transport.write(msgAppReponse.encode(encoding='UTF-8'))
            self.transport.loseConnection()
        elif msgAppReponse == '':
            print(msgErreur)
            self.transport.loseConnection()
        else:
            # Ajout du code de fin de ligne...
            msgAppReponseEnvoye = msgAppReponse + CODE_FIN_LIGNE
            self.transport.write(msgAppReponseEnvoye.encode(encoding='UTF-8'))

###############################################################################


class Interface_serveur:
    """Représentation du serveur"""

    # --- CONSTRUCTEUR ---
    def __init__(self, host, port, format):
        "Se donner une connexion au serveur"

        # XML ou JSON
        self.format = format

        # Se donner un objet de la classe socket.
        self.sock = socket.socket()

        # Fixé à non-blocking
        self.sock.settimeout(None)

        print('Client tente de se connecter au serveur (' + host + ', ' + str(port) + ')...')

        # Connexion au serveur
        self.sock.connect((host, port))

    def __str__(self):
        "Permet de transformer une connexion en chaîne de caractères"
        return '({}, {})'.format(self.format, str(self.sock))

    def verifie_syntaxe(self, msgApp):
        "Analyse la syntaxe du message du client"
        try:
            if self.format == 'xml':
                xml.dom.minidom.parseString(msgApp)
            else:
                json.loads(msgApp)
            reponse = True
        except:
            reponse = False

        return reponse

    def recevoir_texte(self):
        "Attendre une réponse du serveur"

        compteur = 1
        texte_recu_total = ''

        while compteur < MAX_NOMBRE_RECV:
            texte_recu = self.sock.recv(MAX_RECV).decode(encoding='UTF-8')
            # self.sock.settimeout(2.0)
            if compteur > 1:
                print('Attente du contenu... (' + str(compteur) + '/' + str(MAX_NOMBRE_RECV) + ')')
            compteur += 1
            texte_recu_total += texte_recu
            if self.verifie_syntaxe(texte_recu_total):
                break
            else:
                print("Attention, le texte reçu jusqu'à maintenant n'est pas syntaxiquement correct.")
                print("Attente de la suite...")
                print("(Faire CTRL-C s'il y a erreur)")
        if compteur >= MAX_NOMBRE_RECV:
            print("Réponse définitive: le texte du serveur reçu n'est pas syntaxiquement correct.")
            self.fermeture()
            sys.exit(1)

        # self.sock.settimeout(None)

        return texte_recu_total

    def envoi_texte(self, msgClient):
        "Envoyer un texte au serveur"

        self.sock.send(msgClient.encode(encoding='UTF-8'))

    def fermeture(self):
        "Fermeture de la connection avec le serveur"

        self.sock.close()
#!/usr/bin/python3
# -*- coding: utf-8 -*-

############################################################################
# import sys
import xml.dom.minidom

import json

# from serveur.constantes import *

class Protocole:
    """Interface du langage de communication"""

    def __init__(self):
        pass

##########################################################################
class Protocole_json(Protocole):
    """Interface du langage de communication JSON"""

    # --- CONSTRUCTEUR ---
    def __init__(self):
        super(Protocole_json, self).__init__()

        self.dom = None

    def verifie_syntaxe(self, msgClient):
        "Analyse la syntaxe du message du client"
        try:
            self.dom = json.loads(msgClient)
            reponse = True
            # print(json.dumps(self.dom, indent=4))
        except:
            self.dom = None
            reponse = False

        return reponse

    #####################################################################
    def interpreteClient(self, msgApp):
        """Traitement du message correct venant du serveur en JSON
        Retourne un tuple contenant la réponse du serveur et
        le message d'erreur.
        """

        msgReponse = {}

        try:
            msgReponse = json.loads(msgApp)
        except:
            return ('', ERREUR_TRAITEMENT_TEXTE + ' ' + msgApp)

        if msgReponse == {}:
            # print("Le contenu JSON reçu du serveur est non-reconnu ou mal formaté.")
            # sys.exit(1)
            return ('', "Le contenu JSON reçu du serveur est non-reconnu ou mal formaté.")

        return (msgReponse, '')


    ####################################################################
    def interpreteServeur(self, msgApp, serveur):
        """Traitement du message correct venant du client en JSON
        Retourne un tuple contenant la réponse du serveur et
        le message d'erreur.
        """

        msgReponse = None
        msgErreur = ''

        try:
            self.dom = json.loads(msgApp)
        except:
            return ('', ERREUR_TRAITEMENT_TEXTE + ' ' + msgApp)

        # Traitement de { 'salutation': 'bonjourServeur' }
        for node in self.dom:
            if node == SALUTATION and self.dom[node] == BONJOUR_SERVEUR:
                reponse_serveur = serveur.bonjour()
                if reponse_serveur[0] == 1:
                    msgReponse = self.gen_bonjour()
                else:
                    return ('', ERREUR_TRAITEMENT_SALUTATION_CLIENT)

        # Traitement de { "questionNomServeur": "" }
        for node in self.dom:
            if node == QUESTION_NOM_SERVEUR:
                reponse_serveur = serveur.nom()
                if reponse_serveur[0] == 2:
                    # msgReponse = self.gen_nom(reponse_serveur[1])
                    msgReponse = self.gen_nom(NOM_SERVEUR_ACTUEL)
                else:
                    return ('', ERREUR_TRAITEMENT_NOM_SERVEUR)

        # "Traitement de { "listeDossiers": { "dossier": [ "d1/d2/d3", "d1/d3", "d1/d4", … ] } }"
        for node in self.dom:
            if node == QUESTION_LISTE_DOSSIERS:
                dossier = self.dom[node]
                reponse_serveur = serveur.listeDossiers(dossier)
                # On vérifie si on est devant une demande de liste de dossiers
                if reponse_serveur[0] == 3:
                    msgReponse = self.gen_listeDossiers(reponse_serveur[1], reponse_serveur[2])
                else:
                    return ('', ERREUR_TRAITEMENT_LISTE_DOSSIER)

        # Traitement de { 'questionListeFichiers': 'd1' }
        for node in self.dom:
            if node == QUESTION_LISTE_FICHIERS:
                reponse_serveur = serveur.listeFichiers(self.dom[node])
                # On vérifie si on est devant une demande de liste de fichiers
                if reponse_serveur[0] == 4:
                    msgReponse = self.gen_listeFichiers(reponse_serveur[1], reponse_serveur[2])
                else:
                    return ('', ERREUR_TRAITEMENT_LISTE_FICHIERS)

        # Traitement de { "creerDossier" : "d" }
        for node in self.dom:
            if node == CREER_DOSSIER:
                reponse_serveur = serveur.creerDossier(self.dom[node])
                if reponse_serveur[0] == 5:
                    msgReponse = self.gen_creerDossier(reponse_serveur[1])
                else:
                    return ('', ERREUR_TRAITEMENT_CREATION_DOSSIER)

        # { "televerserFichier": { "nom": "...", "dossier": "...", "signature": "…" "contenu": "…" "date": "…" } }
        for node in self.dom:
            if node == TELEVERSER_FICHIER:
                node_interne = self.dom[node]
                nom = ''
                dossier = ''
                signature = ''
                date = '0'
                contenu = ''
                for node2 in node_interne:
                    if node2 == NOM:
                        nom = node_interne[node2]
                    if node2 == DOSSIER:
                        dossier = node_interne[node2]
                    if node2 == SIGNATURE:
                        signature = node_interne[node2]
                    if node2 == CONTENU:
                        contenu = node_interne[node2]
                    if node2 == DATE:
                        date = node_interne[node2]
                reponse_serveur = serveur.televerserFichier(nom, dossier, signature, contenu, date)
                if reponse_serveur[0] == 6:
                    msgReponse = self.gen_televerserFichier(reponse_serveur[1])
                else:
                    return ('', ERREUR_TRAITEMENT_TELEVERSEMENT_FICHIERS)

        # { "telechargerFichier": { "nom": "...", "dossier": "…" } }
        for node in self.dom:
            if node == TELECHARGER_FICHIER:
                node_interne = self.dom[node]
                nom = ''
                dossier = ''
                for node2 in node_interne:
                    if node2 == NOM:
                        nom = node_interne[node2]
                    if node2 == DOSSIER:
                        dossier = node_interne[node2]

                reponse_serveur = serveur.telechargerFichier(nom, dossier)
                if reponse_serveur[0] == 7:
                    signature = reponse_serveur[1]
                    contenu = reponse_serveur[2]
                    date = reponse_serveur[3]
                    erreur = reponse_serveur[4]
                    msgReponse = self.gen_telechargerFichier(signature, contenu, date, erreur)
                else:
                    return ('', ERREUR_TRAITEMENT_TELECHARGEMENT_FICHIERS)

        # Traitement de { "supprimerFichier" : { "nom": "n", "dossier": "d" } }
        for node in self.dom:
            if node == SUPPRIMER_FICHIER:
                node_interne = self.dom[node]
                dossier = ''
                nom = ''
                for node2 in node_interne:
                    if node2 == NOM:
                        nom = node_interne[node2]
                    if node2 == DOSSIER:
                        dossier = node_interne[node2]
                reponse_serveur = serveur.supprimerFichier(nom, dossier)

                if reponse_serveur[0] == 8:
                    msgReponse = self.gen_supprimerFichier(reponse_serveur[1])
                else:
                    return ('', ERREUR_TRAITEMENT_SALUTATION_CLIENT)

        # Traitement de { "supprimerDossier": "d" }
        for node in self.dom:
            if node == SUPPRIMER_DOSSIER:
                dossier = self.dom[node]
                # if dossier != '' and dossier[-1] != '/':
                #     dossier = self.dom[node] + '/'
                reponse_serveur = serveur.supprimerDossier(dossier)
                if reponse_serveur[0] == 9:
                    msgReponse = self.gen_supprimerDossier(reponse_serveur[1])
                else:
                    return ('', ERREUR_TRAITEMENT_SUPPRESSION_DOSSIER)

        # Traitement de { "questionFichierRecent": { "nom": ... "dossier": ... "date": ...}}
        for node in self.dom:
            if node == QUESTION_FICHIER_RECENT:
                node_interne = self.dom[node]
                dossier = ''
                nom = ''
                date = ''
                for node2 in node_interne:
                    if node2 == NOM:
                        nom = node_interne[node2]
                    if node2 == DOSSIER:
                        dossier = node_interne[node2]
                    if node2 == DATE:
                        date = node_interne[node2]
                reponse_serveur = serveur.fichierRecent(nom, dossier, date)
                if reponse_serveur[0] == 10:
                    reponse = reponse_serveur[1]
                    erreur = reponse_serveur[2]
                    msgReponse = self.gen_fichierRecent(reponse, erreur)
                else:
                    return ('', ERREUR_RECHERCHE_FICHIER_PLUS_RECENT)

        # Traitrement de { "questionFichierIdentique": { "nom": ... "dossier": ... "signature": ... "date": ...}}
        for node in self.dom:
            if node == QUESTION_FICHIER_IDENTIQUE:
                node_interne = self.dom[node]
                dossier = ''
                nom = ''
                signature = ''
                date = '0'
                for node2 in node_interne:
                    if node2 == NOM:
                        nom = node_interne[node2]
                    if node2 == DOSSIER:
                        dossier = node_interne[node2]
                    if node2 == SIGNATURE:
                        signature = node_interne[node2]
                    if node2 == DATE:
                        date = node_interne[node2]

                reponse_serveur = serveur.fichierIdentique(nom, dossier, signature, date)
                if reponse_serveur[0] == 11:
                    reponse = reponse_serveur[1]
                    erreur = reponse_serveur[2]
                    msgReponse = self.gen_fichierIdentique(reponse, erreur)
                else:
                    # print("Erreur de recherche de comparaison de fichiers.")
                    # sys.exit(1)
                    return ('', ERREUR_RECHERCHE_COMPARAISON_FICHIERS)

        # Traitement de {"quitter": ""}
        for node in self.dom:
            if node == QUITTER:
                reponse_serveur = serveur.quitter()
                if reponse_serveur[0] == 12:
                    msgReponse = self.gen_quitter()
                    msgErreur = DEMANDE_ARRET_COMMUNICATION
                else:
                    # print("Erreur de l'arrêt du serveur.")
                    # sys.exit(1)
                    return ('', ERREUR_ARRET_SERVEUR)

        if msgReponse is None:
            return ('', CONTENU_JSON_RECU_NONRECONNU_MAL_FORMATE)

        return (msgReponse, msgErreur)

    def gen_bonjour(self):
        "Génère la salutation pour le client"
        rep = {SALUTATION: BONJOUR_CLIENT}
        return json.dumps(rep)

    def gen_nom(self, nom):
        "Génère la réponse du nom du serveur"
        rep = {NOM_SERVEUR: nom}
        return json.dumps(rep)

    def gen_listeDossiers(self, liste_dossiers, erreur):
        # "Génération de { "listeDossiers": { "dossiers": [ "d1/d2/d3", "d1/d3", "d1/d4", … ] } }"

        donneesJSON = ''

        # erreur est un tuple contenant le numéro de l'erreur et sa description.
        # Dossier inexistant.
        if erreur[0] == 1:
            rep = {REPONSE: ERREUR_DOSSIER_INEXISTANT}
            donneesJSON = json.dumps(rep)
        # Erreur de lecture du dossier.
        elif erreur[0] == 2:
            rep = {REPONSE: ERREUR_DOSSIER_LECTURE}
            donneesJSON = json.dumps(rep)
        else:
            rep = {'listeDossiers': {DOSSIERS: liste_dossiers}}
            donneesJSON = json.dumps(rep)

        return donneesJSON

    def gen_listeFichiers(self, liste_fichiers, erreur):
        "Génération de  { LISTE_FICHIERS: { 'fichiers': [ 'd1/f1', 'd1/f2', 'd1/f3' ] } }"

        if erreur[0] == 1:
            rep = {REPONSE: ERREUR_DOSSIER_INEXISTANT}
            donneesJSON = json.dumps(rep)
        else:
            rep = {LISTE_FICHIERS: {FICHIERS: liste_fichiers}}
            donneesJSON = json.dumps(rep)

        return donneesJSON

    def gen_creerDossier(self, erreur):
        "Traitement de la réponse à {'creerDossier' : 'd' }"

        if erreur[0] == 1:
            rep = {REPONSE: ERREUR_DOSSIER_EXISTE}
        elif erreur[0] == 2:
            rep = {REPONSE: ERREUR_DOSSIER_INEXISTANT}
        else:
            rep = {REPONSE: OK}

        return json.dumps(rep)

    def gen_televerserFichier(self, erreur):
        # "Traitement de { "televerserFichier": { "nom": "...", "dossier": "...", "signature": "…" "contenu": "…" "date": "…" } }

        if erreur[0] == 1:
            rep = {REPONSE: ERREUR_FICHIER_EXISTE}
        elif erreur[0] == 2:
            rep = {REPONSE: ERREUR_SIGNATURE}
        elif erreur[0] == 3:
            rep = {REPONSE: ERREUR_DOSSIER_INEXISTANT}
        else:
            rep = {REPONSE: OK}

        return json.dumps(rep)

    def gen_telechargerFichier(self, signature, contenu, date, erreur):
        "Traitement de <telechargerFichier>...</telechargerFichier>"

        if erreur[0] == 1:
            rep = {REPONSE: ERREUR_FICHIER_INEXISTANT}
        elif erreur[0] == 2:
            rep = {REPONSE: ERREUR_FICHIER_LECTURE}
        else:
            rep = {FICHIER: {SIGNATURE: signature,
                               DATE: date,
                               CONTENU: contenu}}
        return json.dumps(rep)

    def gen_supprimerFichier(self, erreur):
        "Traitement de <supprimerFichier>...</supprimerFichier>"

        if erreur[0] == 1:
            rep = {REPONSE: ERREUR_FICHIER_INEXISTANT}
        elif erreur[0] == 2:
            rep = {REPONSE: ERREUR_FICHIER_LECTURE}
        elif erreur[0] == 3:
            rep = {REPONSE: ERREUR_DOSSIER_INEXISTANT}
        else:
            rep = {REPONSE: OK}

        return json.dumps(rep)

    def gen_supprimerDossier(self, erreur):
        "Traitement de <supprimerDossier>...</supprimerDossier>"

        if erreur[0] == 1:
            rep = {REPONSE: ERREUR_DOSSIER_INEXISTANT}
        elif erreur[0] == 2:
            rep = {REPONSE: ERREUR_DOSSIER_LECTURE}
        else:
            rep = {REPONSE: OK}

        return json.dumps(rep)

    def gen_fichierRecent(self, reponse, erreur):
        "Traitement de <questionFichierRecent>...</questionFichierRecent>"

        if erreur[0] == 1:
            rep = {REPONSE: ERREUR_FICHIER_INEXISTANT}
        elif erreur[0] == 2:
            rep = {REPONSE: ERREUR_FICHIER_LECTURE}
        else:
            if reponse[0] == 1:
                rep = {REPONSE: OUI}
            else:
                rep = {REPONSE: NON}

        return json.dumps(rep)

    def gen_fichierIdentique(self, reponse, erreur):
        "Traitement de <questionFichierIdentique>...</questionFichierIdentique>"

        if erreur[0] == 1:
            rep = {REPONSE: ERREUR_FICHIER_INEXISTANT}
        elif erreur[0] == 2:
            rep = {REPONSE: ERREUR_FICHIER_LECTURE}
        else:
            if reponse[0] == 1:
                rep = {REPONSE: OUI}
            else:
                rep = {REPONSE: NON}

        return json.dumps(rep)

    def gen_quitter(self):
        "Quitter la connexion"

        rep = {REPONSE: BYE}
        return json.dumps(rep)
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import hashlib
import os
import binascii

import pathlib

import sys


class SF:
    """Représentation de la gestion du système de fichier"""

    def __init__(self, prefixe='.'):
        """Le chemin d'accès sera gardé avec un '/' à la fin"""

        if prefixe == '':
            self.racine = os.path.abspath('./')
        else:
            prefixe_traite = prefixe.replace("\\", '/')
            if prefixe_traite[-1] == '/':
                self.racine = prefixe_traite
            else:
                self.racine = prefixe_traite + '/'

    def __str__(self):
        "Permet de transformer un SF  en chaîne de caractères"
        return '({})'.format(self.racine)

    def traitement_dossier(self, dossier):
        """Transformation d'un dossier en ajoutant '/' à la fin"""
        dossier_traite = dossier.replace("\\", '/')

        if dossier == '':
            dossier_traite = './'
        elif dossier_traite[-1] != '/':
            dossier_traite = dossier_traite + '/'

        return dossier_traite

    def dossier_existe(self, dossier):
        """Vérifie si un dossier existe.  Remplace os.path.exists"""
        return os.path.exists(dossier)

    def fichier_existe(self, fichier):
        """Vérifie si un fichier existe.  Remplace os.path.exists"""

        return os.path.exists(fichier)

    def dossier_traverse(self, dossier):
        """Remplace os.walk()"""

        return os.walk(dossier)

    def liste_dossiers(self, dossier):
        """Remplace listdir()"""

        liste_dossiers = list()

        dossier_traite = self.traitement_dossier(dossier)

        liste_objets = os.listdir(dossier_traite)

        for nom in liste_objets:
            if os.path.isdir(dossier_traite + nom):
                liste_dossiers.append(nom)

        return liste_dossiers

    def liste_fichiers(self, dossier):
        """Remplace listdir()"""

        liste_fichiers = list()

        dossier_traite = self.traitement_dossier(dossier)

        liste_objets = os.listdir(dossier_traite)

        for nom in liste_objets:
            if os.path.isfile(dossier_traite + nom):
                liste_fichiers.append(nom)

        return liste_fichiers

    def creer_fichier(self, nom, dossier, signature, contenu, date):
        "Action de créer le fichier sur le système de fichier"


        dossier_traite = self.traitement_dossier(dossier)
        fichier = dossier_traite + nom

        if self.fichier_existe(fichier):
            print('Erreur: le fichier "' + fichier + '" ne devrait pas exister...')
            sys.exit(1)
        else:
            try:
                contenu_decode = binascii.a2b_base64(contenu)
            except:
                contenu += '=' * (-len(contenu) % 4)
                contenu_decode = binascii.a2b_base64(contenu)

            # m = hashlib.md5()
            m = hashlib.sha256()
            m.update(contenu_decode)
            sign_recu = m.hexdigest()

            if signature != sign_recu:
                print('Erreur: le fichier "' + fichier + "\" n'a pas une bonne signature")
                sys.exit(1)
            else:
                try:
                    obj_fichier = self.ouvrir(fichier, 'wb')
                    obj_fichier.write(contenu_decode)
                    obj_fichier.close()

                    self.fixer_modif_date(fichier, float(date))

                except:
                    # TODO: Ajouter une erreur de type "Erreur fichier ecriture"
                    print("Erreur: impossible d'écrire le fichier \"" + fichier + "\" sur le système de fichier.")
                    sys.exit(1)

    def creer_dossier(self, dossier):
        """Remplace os.mkdir()"""

        return os.mkdir(dossier)

    def stat(self, fichier):
        """Remplace os.stat()"""

        return os.stat(fichier)

    def ouvrir(self, fichier, mode='r'):
        """Remplace open()"""

        return open(fichier, mode)

    def utime(self, fichier, date):
        """Remplace os.utime"""

        return os.utime(fichier, date)

    def supprime_fichier(self, fichier):
        """Remplace os.remove()"""

        return os.remove(fichier)

    def supprime_dossier(self, dossier):
        """Remplace os.remove()"""

        return os.rmdir(dossier)

    def extraire_fichier(self, dossier_fichier):
        p = pathlib.Path(dossier_fichier)
        return p.name

    def extraire_dossier(self, dossier_fichier):
        p = pathlib.Path(dossier_fichier)
        return str(p.parent)

    def obtenir_signature(self, fichier):
        "Retourne la signature d'un fichier local"
        try:
            contenu_lu = self.ouvrir(fichier, 'rb').read()
        except:
            print("Erreur de lecture du fichier '" + fichier + "' pour obtention de la signature...")
            sys.exit(1)

        # m = hashlib.md5()
        m = hashlib.sha256()
        m.update(contenu_lu)

        return m.hexdigest()

    def obtenir_signature_contenu(self, contenu):
        "Retourne la signature d'un contenu en paramètre"

        contenu_lu = contenu

        # m = hashlib.md5()
        m = hashlib.sha256()
        m.update(contenu_lu)

        return m.hexdigest()

    def obtenir_date(self, fichier):
        "Retourne la date de modification d'un fichier local"
        try:
            fichier_stat = self.stat(fichier)
        except:
            print("Erreur de lecture du fichier '" + fichier + "' pour obtention de la date...")
            sys.exit(1)
        return fichier_stat.st_mtime

    def fixer_modif_date(self, fichier, date_modif):
        try:
            fichier_stat = self.stat(fichier)
            date_acces = fichier_stat.st_atime
            os.utime(fichier,(date_acces,date_modif))
        except:
            print("Erreur: impossible d'écrire la date " + str(date_modif) + " du fichier " + fichier)
            sys.exit(1)

    def obtenir_contenu(self, fichier):
        "Retourne le contenu d'un fichier local"
        try:
            contenu = self.ouvrir(fichier, 'rb').read()
        except:
            print("Erreur de lecture du fichier '" + fichier + "' pour obtention du contenu...")
            sys.exit(1)

        contenu_encode = binascii.b2a_base64(contenu)
        contenu_ascii = ''
        if contenu_encode != '':
            contenu_ascii = contenu_encode.decode(encoding='ascii')
        return contenu_ascii


#!/usr/bin/python3
# -*- coding: utf-8 -*-

##############################################################################################
# import base64
# import hashlib
# import os

import math

import binascii

from twisted.internet.protocol import Factory

# import serveur.sf

# from serveur.constantes import *

class Serveur(Factory):
    """Squelette du serveur"""


    # --- CONSTRUCTEUR ---
    def __init__(self, proto):
        "Constructeur du serveur et attendre une connection"

        # Prise en charge de l'objet qui effectuera la conversion vers le langage XML ou JSON.
        self.proto = proto

        # Construction d'un système de fichier
        # self.sf = serveur.sf.SF(DOSSIER_RACINE)
        self.sf = SF(DOSSIER_RACINE)

    def bonjour(self):
        "Traitement de Bonjour Serveur"

        # return self.proto.gen_bonjour()
        return (1, BONJOUR)


    def nom(self):
        "Traitement de la demande du nom du serveur"

        return (2, NOM_SERVEUR)

    def listeDossiers(self, dossier):
        "Traitement du liste d'un dossier"

        liste_dossiers = list()

        # Aucune erreur au départ.
        erreur = (0, AUCUNE)

        dossier_traite = self.sf.traitement_dossier(dossier)

        if not self.sf.dossier_existe(dossier_traite):

            # Dossier inexistant.
            erreur = (1, DOSSIER_INEXISTANT)
        else:
            try:
                liste_dossiers = self.sf.liste_dossiers(dossier_traite)
            except:
                # Erreur de lecture du dossier.
                erreur = (2, ERREUR_LECTURE_DOSSIER)

        return (3, liste_dossiers, erreur)

    def listeFichiers(self, dossier):
        "Traitement de la liste des fichiers contenus dans un dossier"

        listeFichiers = list()


        # Aucune erreur au départ.
        erreur = (0, "Aucune")

        dossier_traite = self.sf.traitement_dossier(dossier)

        try:
            listeFichiers = self.sf.liste_fichiers(dossier_traite)
        except:
            erreur = (1, DOSSIER_INEXISTANT)

        return (4, listeFichiers, erreur)

    def creerDossier(self, dossier):
        "Création d'un dossier"

        erreur = (0, "Aucune")
        dossier_traite = self.sf.traitement_dossier(dossier)

        if self.sf.dossier_existe(dossier_traite):
            erreur = (1, ERREUR_DOSSIER_EXISTE)
        else:
            try:
                self.sf.creer_dossier(dossier_traite)
            except:
                erreur = (2, ERREUR_DOSSIER_INEXISTANT)

        return (5, erreur)

    def televerserFichier(self, nom, dossier, sign_recue, contenu_recu, date):
        "Téléversement d'un fichier"

        erreur = (0, "Aucune")
        try:
            contenu_decode = binascii.a2b_base64(contenu_recu)
        except:
            contenu_recu += '=' * (-len(contenu_recu) % 4)
            contenu_decode = binascii.a2b_base64(contenu_recu)

        dossier_traite = self.sf.traitement_dossier(dossier)
        fichier = dossier_traite + nom


        if self.sf.fichier_existe(fichier):
            erreur = (1, ERREUR_FICHIER_EXISTE)
        else:
            sign_client = self.sf.obtenir_signature_contenu(contenu_decode)
            if sign_recue != sign_client:
                erreur = (2, ERREUR_SIGNATURE)
            else:
                try:
                    obj_fichier = self.sf.ouvrir(fichier, 'wb')
                    obj_fichier.write(contenu_decode)
                    obj_fichier.close()

                    self.sf.fixer_modif_date(fichier, float(date))

                except:
                    # TODO: Ajouter une erreur de type "Erreur fichier ecriture"
                    erreur = (3, ERREUR_DOSSIER_INEXISTANT)

        return (6, erreur)

    def telechargerFichier(self, nom, dossier):
        "Traitement du téléchargement d'un fichier"

        erreur = (0, AUCUNE)
        sign_serveur = ''
        date_modif = ''

        contenu_ascii = ''

        dossier_traite = self.sf.traitement_dossier(dossier)
        fichier = dossier_traite + nom

        if not self.sf.fichier_existe(fichier):
            erreur = (1, ERREUR_FICHIER_INEXISTANT)
        else:

            contenu_ascii = self.sf.obtenir_contenu(fichier)

            date_modif = self.sf.obtenir_date(fichier)

            sign_serveur = self.sf.obtenir_signature(fichier)

        return (7, sign_serveur, contenu_ascii, str(date_modif), erreur)

    def supprimerFichier(self, nom, dossier):
        "Traitement de <supprimerFichier>...</supprimerFichier>"

        erreur = (0, AUCUNE)

        dossier_traite = self.sf.traitement_dossier(dossier)
        fichier = dossier_traite + nom

        if not self.sf.dossier_existe(dossier_traite):
            erreur = (3, ERREUR_DOSSIER_INEXISTANT)
        elif not self.sf.fichier_existe(fichier):
            erreur = (1, ERREUR_FICHIER_INEXISTANT)
        else:
            try:
                self.sf.supprime_fichier(fichier)
            except:
                erreur = (2, ERREUR_FICHIER_LECTURE)

        return (8, erreur)

    def supprimerDossier(self, dossier):
        "Traitement de <supprimerDossier>...</supprimerDossier>"

        erreur = (0, "Aucune")

        dossier_traite = self.sf.traitement_dossier(dossier)

        if not self.sf.dossier_existe(dossier_traite):
            erreur = (1, ERREUR_DOSSIER_INEXISTANT)
        else:
            try:
                self.sf.supprime_dossier(dossier_traite)
            except:
                erreur = (2, ERREUR_DOSSIER_LECTURE)

        return (9, erreur)

    def fichierRecent(self, nom, dossier, date_client):
        "Vérification si un fichier est plus récent"

        reponse = (2, NON)
        erreur = (0, AUCUNE)
        fichier_stat = 0

        dossier_traite = self.sf.traitement_dossier(dossier)
        fichier = dossier_traite + nom

        if not self.sf.dossier_existe(dossier_traite):
            erreur = (1, ERREUR_FICHIER_INEXISTANT)
        elif not self.sf.fichier_existe(fichier):
            erreur = (1, ERREUR_FICHIER_INEXISTANT)
        else:
            try:
                fichier_stat = self.sf.stat(fichier)
            except:
                erreur = (2, ERREUR_FICHIER_LECTURE)
                fichier_stat = 0

        if fichier_stat != 0:

            date_serveur = fichier_stat.st_mtime

            if math.floor(float(date_client)) > math.floor(date_serveur):
                reponse = (1, "Oui")
            else:
                reponse = (2, "Non")

        return (10, reponse, erreur)

    def fichierIdentique(self, nom, dossier, signature, date_client):
        "Vérification si un fichier est identique sur le serveur"

        reponse = (1, OUI)
        erreur = (0, AUCUNE)

        dossier_traite = self.sf.traitement_dossier(dossier)
        fichier = dossier_traite + nom

        if not self.sf.dossier_existe(dossier_traite):
            erreur = (1, ERREUR_FICHIER_INEXISTANT)
        elif not self.sf.fichier_existe(fichier):
            erreur = (1, ERREUR_FICHIER_INEXISTANT)
        else:
            sign_serveur = self.sf.obtenir_signature(fichier)

            date_serveur = self.sf.obtenir_date(fichier)


            if math.floor(float(date_client)) == math.floor(date_serveur):
                reponse = (1, OUI)

                if sign_serveur != signature:
                    reponse = (2, NON)
            else:
                reponse = (2, NON)

        return (11, reponse, erreur)

    def quitter(self):
        "Traitement de l'opération quitter."

        return (12, QUITTER)
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

# import serveur.serveur
# import serveur.protocole
# import serveur.interface
# import serveur.mock_interface
# import serveur.synchronisation

# from twisted.internet import reactor, protocol
from twisted.internet import reactor

###############################################################################
# Main #
###############################################################################
if __name__ == '__main__':

    if len(sys.argv) <= 1:
        # print("Ce serveur prend le numéro du port en paramètre et le protocole (xml, json).")
        print("Ce serveur prend le numéro du port en paramètre.")
        sys.exit(1)

    # Récurérer le numéro du port en paramètre du serveur.
    port = int(sys.argv[1])

    # xml ou json
    # commande_protocole = sys.argv[2]
    commande_protocole = "json"

    # Dossier racine
    racine = './'

    # Object permettant de traiter le format XML ou JSON.
    if commande_protocole == 'json':
        # proto = serveur.protocole.Protocole_json()
        proto = Protocole_json()
    else:
        print("Le 2ième paramètre doit être le protocole (xml, json).")
        sys.exit(1)

    # Création d'une "Factory" pour le serveur Twisted.
    # factory = serveur.serveur.Serveur(proto)
    factory = Serveur(proto)
    # factory.protocol = serveur.interface.Interface_client
    factory.protocol = Interface_client
    reactor.listenTCP(port, factory)
    print("Le serveur est en attente d'une connexion...")
    reactor.run()
