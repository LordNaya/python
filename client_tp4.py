#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socket
import sys
import json

from interpreteClient import InterpreteClient
from interpretreServeur import InterpreteServeur

END_STRING = "\r\n"
MAX_RECV = 1024 * 1024 * 512

    
def client_program():
    host = ''
    port = 50000
    client_socket = socket.socket()
    client_socket.connect((host, port))
    

    interpreteClient = InterpreteClient()
    interpretreServeur = InterpreteServeur()

    message = input(" -> ")
    
    while True:     
 
 #  Traitement de la commande du client  

        messageConverti = interpreteClient.obtenirJsonMsg(message)
        msgAConvertirEnJson = messageConverti[0]
      
#   Envoit vers serveur  

        jssssooooonnnnnn = json.dumps(msgAConvertirEnJson).encode('utf-8')
        client_socket.sendall(jssssooooonnnnnn)
        client_socket.sendall(END_STRING.encode('utf-8')) 
       
 #  Reception message serveur  

        data = client_socket.recv(MAX_RECV).decode(encoding='UTF-8')

#   Traitement message serveur 
     
        reponseServeur = interpretreServeur.obtenirMsgServeurSelonJson(data)
        print(reponseServeur[0])

 #  Break or no break    
 
        if reponseServeur[0] == "Bye.":    
            break
        else:
            message = input(" -> ") 

#   Fermeture du socket

    client_socket.close()

if __name__ == '__main__':
    client_program()

