#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
from unittest.mock import MagicMock
from interpreteClient import InterpreteClient 

class TestInterpreteClient(unittest.TestCase):

    def test_obtenirCommande_AvecAucunDossier_AvecAucunFichier(self):
        commande = "connecter?"
        expected = ['connecter', '']

        retour = InterpreteClient.obtenirCommande(self, commande)

        self.assertEqual(retour, expected)

    def test_obtenirCommande_AvecDossier_AvecFichier(self):
        commande = "televerser?d1/d2/d3/fichier1"
        expected = ['televerser', 'd1/d2/d3/fichier1']

        retour = InterpreteClient.obtenirCommande(self, commande)

        self.assertEqual(retour, expected)


    def test_obtenirListeDossiers_AvecListeDossiers_AvecFichier_RetourneListeDossierSeulement(self):
        commande = "d1/d2/d3/fichier1"
        expected = ['d1', 'd2', 'd3']

        retour = InterpreteClient.obtenirListeDossiers(self, commande)

        self.assertEqual(retour, expected)

    def test_obtenirListeFichiers_AvecListeDossiers_AvecListeFichiers_RetourneListeFichiersSeulement(self):
        commande = "d1/d2/d3/fichier1/fichier2/fichier3"
        expected = ['fichier1', 'fichier2', 'fichier3']

        retour = InterpreteClient.obtenirlisteFichiers(self, commande)

        self.assertEqual(retour, expected)

    def test_convertirListeDossierVersString_AvecListeDossiers_RetourneStringFormater(self):
        commande = ['d1', 'd2', 'd3']
        expected = "d1/d2/d3"

        retour = InterpreteClient.convertirListeDossierVersString(self, commande)

        self.assertEqual(retour, expected)

    def test_generate_bonjour(self):
        expected = {"salutation": "bonjourServeur"}

        retour = InterpreteClient.generate_bonjour(self)

        self.assertEqual(expected, retour)  

    def test_generate_nomServeur(self):
        expected = {"questionNomServeur": ""}

        retour = InterpreteClient.generate_nomServeur(self)

        self.assertEqual(expected, retour)  

    def test_generate_create_dossier(self):
        listeDossier = ['d1', 'd2', 'd3']
        expected = {"creerDossier": "d1/d2/d3"}

        retour = InterpreteClient.generate_create_dossier(self, listeDossier)

        self.assertEqual(expected, retour)

    # def test_generate_upload(self):
    #     listeDossier = ['d1', 'd2', 'd3']
    #     listeFichier = ['fichier1']
    #     expected = { "televerserFichier": 
    #                     {
    #                         "nom": "fichier1",
    #                         "dossier": "d1/d2/d3",
    #                         "signature": "",
    #                         "contenu": "",
    #                         "date": ""
    #                     }
    #                 }  

    #     retour = InterpreteClient.generate_upload(self, listeDossier, listeFichier)

        # self.assertEqual(expected, retour)
    

    def test_generate_download(self):
        listeDossier = ['d1', 'd2', 'd3']
        listeFichier = ['fichier1']
        expected = {"telechargerFichier": {"nom": "fichier1", "dossier": "d1/d2/d3"}}

        retour = InterpreteClient.generate_download(self, listeDossier, listeFichier)

        self.assertEqual(expected, retour)

    def test_generate_suppression_dossier(self):
        listeDossier = ['d1', 'd2', 'd3']
        expected = {"supprimerDossier": "d1/d2/d3"}

        retour = InterpreteClient.generate_suppression_dossier(self, listeDossier)

        self.assertEqual(expected, retour)

    def test_generate_suppression_fichier(self):
        listeDossier = ['d1', 'd2', 'd3']
        listeFichier = ['fichier1', 'fichier2']

        expected = {"supprimerFichier": {"nom": "fichier1/fichier2", "dossier": "d1/d2/d3"}}

        retour = InterpreteClient.generate_suppression_fichier(self, listeDossier, listeFichier)

        self.assertEqual(expected, retour)

    # def test_generate_fichier_identique(self):
    #     listeDossier = ['d1', 'd2', 'd3']
    #     listeFichier = ['fichier1', 'fichier2']

    #     expected = expected = { "questionFichierIdentique": 
    #                     {
    #                         "nom": "fichier1/fichier2",
    #                         "dossier": "d1/d2/d3",
    #                         "signature": "",
    #                         "date": ""
    #                     }
    #                 }  

    #     retour = InterpreteClient.generate_fichier_identique(self, listeDossier, listeFichier)

    #     self.assertEqual(expected, retour)

    # def test_generate_fichier_recent(self):
    #     listeDossier = ['d1', 'd2', 'd3']
    #     listeFichier = ['fichier1', 'fichier2']

    #     expected = expected = { "questionFichierRecent": 
    #                     {
    #                         "nom": "fichier1/fichier2",
    #                         "dossier": "d1/d2/d3",
    #                         "date": ""
    #                     }
    #                 }  

    #     retour = InterpreteClient.generate_fichier_recent(self, listeDossier, listeFichier)

    #     self.assertEqual(expected, retour)


    def test_generate_quitter(self):
        expected = {"quitter": ""}

        retour = InterpreteClient.generate_quitter(self)

        self.assertEqual(expected, retour)  


    # def test_obtenirJsonMsg(self):
    #     expected = ('', 'La commande n\'est pas reconnue par le serveur.')
    #     commande = "La patate est le meilleur Legume. FIght me."

    #     retour = InterpreteClient.obtenirJsonMsg(self, commande)

    #     self.assertEqual(expected, retour)

if __name__ == '__main__':
    unittest.main()        