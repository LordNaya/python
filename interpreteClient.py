#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import dictionnaireConst as const
from theHeroOfTime import Hero_of_Time
from leNotaire import Notaire


class InterpreteClient:

    def __init__(self):
        pass

    def obtenirCommande(self, rawInput):
       return rawInput.split('?')

    def obtenirListeDossiers(self, clientRequest):  
        array = clientRequest.split('/') 
        listeDossier = []    

        for value in array:
            if value[0] == "d":
                listeDossier.append(value)
            else:
                pass

        return listeDossier

    def obtenirlisteFichiers(self, clientRequest):
        array = clientRequest.split('/') 
        listeFichiers = []    

        for value in array:
            if value[0] == "f":
                listeFichiers.append(value)    
            else:
                pass

        return listeFichiers  

    def convertirListeDossierVersString(self, listeDossiers):
        retour = ""
        for value in listeDossiers:
            retour += value
            retour += "/" 

        return retour[:-1]

# BIG ASS IF ELIF ELSE

    def obtenirJsonMsg(self, commandeClient):
        msgClient = ""
        msgErreur = ""
        fichiers = None
        dossiers = None

        msg = self.obtenirCommande(commandeClient)

        commande = msg[0]

        if msg[0] != "quitter":
            if msg[1] != "":
                fichiers = self.obtenirlisteFichiers(msg[1])
                dossiers = self.obtenirListeDossiers(msg[1])
 
        # Traitement de "connecter?"  
        if commande == const.CONNECTER:
            msgClient = self.generate_bonjour()

        # Traitement de "nomServeur?"  
        elif commande == const.NOM_SERVEUR:
            msgClient = self.generate_nomServeur()

        # Traitement de "ListeDossier?"  
        elif commande == const.LISTE_DOSSIER:
            if dossiers:
                msgClient = self.generate_liste_Dossiers(dossiers)    

        #Traitement de "dossier?"
        elif commande == const.DOSSIER:
            if dossiers:
                msgClient = self.generate_liste_Dossiers(dossiers)   

        #Traitement de "creerDossier?"
        elif commande == const.CREER:
            if dossiers:
                msgClient = self.generate_create_dossier(dossiers)

        #Traitement de "televerser?"
        elif commande == const.TELEVERSER:
            if dossiers and fichiers:
                msgClient = self.generate_upload(dossiers, fichiers)

        #Traitement de "telecharger?"
        elif commande == const.TELECHARGER_FICHIER: 
            if dossiers and fichiers:
                msgClient = self.generate_download(dossiers, fichiers)    
        
        #Traitement de "supprimerDossier?"
        elif commande == const.SUPPRIMER_DOSSIER:
            if dossiers:
                msgClient = self.generate_suppression_dossier(dossiers)  

        #Traitement de "supprimerDossier?"
        elif commande == const.SUPPRIMER_FICHIER:
            if dossiers and fichiers:
                msgClient = self.generate_suppression_fichier(dossiers, fichiers) 

        #Traitement de "fichierIdentique? et identiqueFichier?"
        elif commande == const.FICHIER_IDENTIQUE or commande == const.IDENTIQUE_FICHIER:
            if dossiers and fichiers:   
                msgClient = self.generate_fichier_identique(dossiers, fichiers) 

        #Traitement de "fichierRecent?"
        elif commande == const.FICHIER_RECENT: 
            if dossiers and fichiers:
                msgClient = self.generate_fichier_recent(dossiers, fichiers)

        #Traitement de "miseAjour"



        #Traitement de "quitter"    
        elif commande == const.QUITTER:
            msgClient = self.generate_quitter()    

        else:
            msgErreur = const.ERREUR_COMMANDE_CLIENT
       
        return (msgClient, msgErreur)


# GENERATEURS JSON 

    def generate_bonjour(self):
        rep = {const.SALUTATION: const.BONJOUR_SERVEUR}
        return rep

    def generate_nomServeur(self):
        rep = {const.QUESTION_NOM_SERVEUR: ""}
        return rep

    def generate_liste_Dossiers(self, listeDossiers):
        listeDossiers = '/'.join(listeDossiers)
        rep = {const.QUESTION_LISTE_DOSSIERS: listeDossiers}
        return rep

    def generate_create_dossier(self, listeDossier):
        listeDossiers = '/'.join(listeDossier)
        rep = {const.CREER_DOSSIER: listeDossiers}
        return rep

##################################################################################
    def generate_upload(self, listeDossier, listeFichier):
        contenuString = Notaire.obtenirContenuString(self, listeFichier[0])
        signature = Notaire.obtenir_signature_contenu(self, listeFichier[0])
        repertoire = self.convertirListeDossierVersString(listeDossier)

        rep = {  
                const.TELEVERSER_FICHIER: 
                    {
                        const.NOM: listeFichier[0], 
                        const.DOSSIER: repertoire, 
                        const.SIGNATURE: signature,
                        const.CONTENU: contenuString,
                        const.DATE: Hero_of_Time.obtenirDateHeure(self), 
                    }
        }
        return rep        
#########################################################################################
    
    def generate_download(self, listeDossier, listeFichier):
        listeDossier = '/'.join(listeDossier)
        rep = {  
                const.TELECHARGER_FICHIER: 
                    {
                        const.NOM: listeFichier[0], 
                        const.DOSSIER: listeDossier, 
                    }
        }
        return rep

    def generate_suppression_dossier(self, listeDossier):
        liste = '/'.join(listeDossier)
        rep = { const.SUPPRIMER_DOSSIER: liste }
        return rep

    def generate_suppression_fichier(self, listeDossier, listeFichier):
        listeDossier = '/'.join(listeDossier)
        listeFichier = '/'.join(listeFichier)

        rep = {  
                const.SUPPRIMER_FICHIER: 
                    {
                        const.NOM: listeFichier, 
                        const.DOSSIER: listeDossier 
                    }
        }
        return rep    

    def generate_fichier_identique(self, listeDossier, listeFichier):
        signature = Notaire.obtenir_signature_contenu(self, listeFichier[0])
        listeDossier = '/'.join(listeDossier)
        listeFichier = '/'.join(listeFichier)
        
        rep = {
                const.QUESTION_FICHIER_IDENTIQUE:
                {
                    const.NOM: listeFichier, 
                    const.DOSSIER: listeDossier, 
                    const.SIGNATURE: signature,
                    const.DATE: Hero_of_Time.obtenirDateHeure(self) 
                }
        }
        return rep

    def generate_fichier_recent(self, listeDossier, listefichier):
        listeDossier = '/'.join(listeDossier)
        listefichier = '/'.join(listefichier)

        rep = {
                const.QUESTION_FICHIER_RECENT:
                {
                    const.NOM: listefichier, 
                    const.DOSSIER: listeDossier, 
                    const.DATE: Hero_of_Time.obtenirDateHeure(self) 
                }
        }
        return rep

    def generate_quitter(self):
        return {const.QUITTER : ""}
