#!/usr/bin/python3
# -*- coding: utf-8 -*-

import datetime


class Hero_of_Time:

    def __init__(self):
        pass


    def obtenirDateHeure(self):        
        return datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S") 
        

    def chop_microseconds(self, delta):
        return delta - datetime.timedelta(microseconds=delta.microseconds)    



  
  
#timeDelta pour la diff/rence de temps entre 2 dateTime

